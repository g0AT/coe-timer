package grafics;

import data.Item;
import logic.UserInterface;

public class UIConsole implements UserInterface {

    private int cnt = 4;
    private Item currentSelected = null;

    @Override
    public Item getSelected() {
        return currentSelected;
    }

    @Override
    public void setData(Item[] items) {
        if (items.length != 3) {
            throw new IllegalArgumentException();
        }
        System.out.printf("%s | %s | %s\n", items[0], items[1], items[2]);
        cnt--;
    }

    @Override
    public boolean isStopped() {
        return (cnt <= 0);
    }

    @Override
    public void reportError(Exception e) {
        e.printStackTrace();
    }

    @Override
    public void pause() throws InterruptedException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
