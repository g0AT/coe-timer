package grafics;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import data.ConstantPool;
import data.Item;
import logic.UserInterface;

public class Gui extends JFrame implements UserInterface {

    private static final long serialVersionUID = -4014498374601548200L;
    private final JPanel panel;
    private final JComboBox<String> dropdown;
    private final JLabel left;
    private final JLabel center;
    private final JLabel right;
    private final JButton startStop;

    private final Font SIDEFONT = new Font("sansSerif", Font.PLAIN, 20);
    private final Font CENTERFONT = new Font("sansSerif", Font.BOLD, 36);

    private final Lock pause;
    private final Condition pauseCond;

    private boolean stopped = true;

    public Gui() {
        super(ConstantPool.FRAMETITLE);

        pause = new ReentrantLock();
        pauseCond = pause.newCondition();

        panel = new JPanel();
        panel.setLayout(new BorderLayout());
        this.add(panel);

        String[] dropDownText = getDropdownText();
        dropdown = new JComboBox<>(dropDownText);
        panel.add(dropdown, BorderLayout.PAGE_START);

        left = new JLabel("");
        left.setFont(SIDEFONT);
        panel.add(left, BorderLayout.LINE_START);
        center = new JLabel("", SwingConstants.CENTER);
        center.setFont(CENTERFONT);
        panel.add(center, BorderLayout.CENTER);
        right = new JLabel("");
        right.setFont(SIDEFONT);
        panel.add(right, BorderLayout.LINE_END);
        initLabels();

        startStop = new JButton("Start");
        startStop.addActionListener((e) -> {
            pause.lock();
            if (stopped) {
                startStop.setText("Stop");
                stopped = false;
            } else {
                startStop.setText("Start");
                stopped = true;
            }
            pauseCond.signal();
            pause.unlock();
        });
        panel.add(startStop, BorderLayout.PAGE_END);

        this.setVisible(true);
        this.setResizable(false);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(new Dimension(400, 150));
    }

    @Override
    public Item getSelected() {
        int selectedIdx = dropdown.getSelectedIndex();
        return Item.values()[selectedIdx];
    }

    @Override
    public void setData(Item[] items) {
        if (items.length != ConstantPool.NODISPLAYED) {
            reportError(new IllegalArgumentException("setData != NODISPLAYED items"));
        } else {
            left.setText(items[Item.LEFT].getName());
            center.setText(items[Item.CENTER].getName());
            right.setText(items[Item.RIGHT].getName());
        }
    }

    @Override
    public synchronized boolean isStopped() {
        return stopped;
    }

    @Override
    public void reportError(Exception e) {
        e.printStackTrace();
    }

    private String[] getDropdownText() {
        Item[] items = Item.values();
        String[] texts = new String[items.length];
        for (int i = 0; i < items.length; i++) {
            texts[i] = items[i].getName();
        }
        return texts;
    }

    private void initLabels() {
        Item[] items = Item.values();
        if (items.length < ConstantPool.NODISPLAYED) {
            reportError(new IllegalArgumentException("not enugh items in list"));
        }
        Item[] set = new Item[ConstantPool.NODISPLAYED];
        for (int i = 0; i < ConstantPool.NODISPLAYED; i++) {
            int idx = (items.length - 1 + i) % items.length;
            set[i] = items[idx];
        }
        setData(set);
    }

    @Override
    public void pause() throws InterruptedException {
        pause.lock();
        while (isStopped()) {
            pauseCond.await();
        }
        pause.unlock();
    }
}
