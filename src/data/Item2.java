package data;

import java.awt.Color;

public enum Item2 {

    PURPLE("lila", Color.magenta),
    LIGHTBLUE("hellblau", new Color(153, 255, 255)),
    RED("rot", Color.red),
    YELLOW("gelb", Color.yellow),
    DARKBLUE("dunkelblau", Color.BLUE),
    BROWN("brau", new Color(102, 51, 0)),
    GREEN("gr�n", Color.green);

    private final String name;
    private final Color color;

    public static final int LEFT = 0;
    public static final int CENTER = 1;
    public static final int RIGHT = 2;

    private Item2(String name, Color color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public Color getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "Items{" + "name=" + name + ", color=" + color + '}';
    }
}
