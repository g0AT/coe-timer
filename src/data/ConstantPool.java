package data;

public class ConstantPool {
    public static final String CFGPATH = "./ressources/config/cfg.xml";
    
    public static final String FRAMETITLE = "Convention of Elements - Timer";
    public static final String AUDIOPATH = "./ressources/sounds/beep.wav";
    public static final int NODISPLAYED = 3;
    public static final int THREADTIMEOUT = 1000;
}
