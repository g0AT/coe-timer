package data;

import java.awt.Color;

import logic.ConfigLoader;

public class Item {
    public static final int LEFT = 0;
    public static final int CENTER = 1;
    public static final int RIGHT = 2;

    private static final Item[] values;

    private final String name;
    private final Color color;

    static {
        values = ConfigLoader.getItemsFromXML();
    }

    public static Item create(String name, int red, int green, int blue) {
        return new Item(name, new Color(red, green, blue));
    }

    public static Item[] values() {
        return values;
    }

    private Item(String name, Color color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public Color getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "Item [name=" + name + ", color=" + color + "]";
    }
}
