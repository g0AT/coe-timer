package logic;

import data.Item;


public interface UserInterface {
    Item getSelected();
    void setData(Item[] items);
    boolean isStopped();
    void reportError(Exception e);
    void pause() throws InterruptedException;
}
