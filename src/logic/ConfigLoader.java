package logic;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import data.ConstantPool;
import data.Item;

public class ConfigLoader {

    private static final int REDIDX = 0;
    private static final int GREENIDX = 1;
    private static final int BLUEIDX = 2;

    private static final File cfgFile = new File(ConstantPool.CFGPATH);
    private static final DocumentBuilderFactory docBuilderFac = DocumentBuilderFactory.newInstance();
    private static DocumentBuilder docBuilder;
    private static Document doc;

    static {
        try {
            docBuilder = docBuilderFac.newDocumentBuilder();
            doc = docBuilder.parse(cfgFile);
            doc.getDocumentElement().normalize();
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }

    public static Item[] getItemsFromXML() {
        NodeList nList = doc.getElementsByTagName("item");
        int length = nList.getLength();
        Item[] items = new Item[length];
        for (int i = 0; i < length; i++) {
            Node n = nList.item(i);
            if (n.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) n;
                items[i] = createItem(n.getTextContent(), elem.getAttribute("color"));
            }
        }
        return items;
    }

    private static Item createItem(String name, String colorString) {
        String[] colorStringSplit = colorString.split(",");
        int red = Integer.parseInt(colorStringSplit[REDIDX]);
        int green = Integer.parseInt(colorStringSplit[GREENIDX]);
        int blue = Integer.parseInt(colorStringSplit[BLUEIDX]);
        return Item.create(name, red, green, blue);
    }

//    public static void main(String[] args) {
//        for (Item item : getItemsFromXML()) {
//            System.out.println(item);
//        }
//    }
}
