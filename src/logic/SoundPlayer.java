package logic;

import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class SoundPlayer {

    private AudioInputStream audioStream;
    private AudioFormat audioFormat;
    private int audioSize;
    private byte[] audioData;
    private DataLine.Info audioInfo;
    private Clip clip;

    private final UserInterface ui;

    public SoundPlayer(String path, UserInterface ui) {
        this.ui = ui;
        try {
            File file = new File(path);
            audioStream = AudioSystem.getAudioInputStream(file);
            audioFormat = audioStream.getFormat();
            audioSize = (int) (audioFormat.getFrameSize() * audioStream.getFrameLength());
            audioData = new byte[audioSize];
            audioInfo = new DataLine.Info(Clip.class, audioFormat, audioSize);
            audioStream.read(audioData, 0, audioSize);
        } catch (UnsupportedAudioFileException | IOException e) {
            ui.reportError(e);
        }
    }

    public void play() {
        try {
            clip = (Clip) AudioSystem.getLine(audioInfo);
            clip.open(audioFormat, audioData, 0, audioSize);
            clip.start();
        } catch (LineUnavailableException ex) {
            ui.reportError(ex);
        }
    }
}
