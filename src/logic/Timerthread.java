package logic;

import data.ConstantPool;
import data.Item;

public class Timerthread extends Thread {

    private final UserInterface ui;
    private final SoundPlayer player;
    private int cIdx = 0;

    public Timerthread(UserInterface ui, SoundPlayer player) {
        this.ui = ui;
        this.player = player;
    }

    @Override
    public void run() {
        Item[] items = Item.values();
        Item[] set = new Item[ConstantPool.NODISPLAYED];
        while (true) {
            try{
                ui.pause();
                cIdx = 0;
            }catch(InterruptedException e){
                break;
            }
            while (!ui.isStopped()) {
                int idx = cIdx + items.length;
                for (int i = 0; i < ConstantPool.NODISPLAYED; i++) {
                    set[i] = items[(idx - 1 + i) % items.length];
                }
                ui.setData(set);
                if (items[cIdx] == ui.getSelected()) {
                    player.play();
                }
//                System.out.println("cIdx = " + cIdx);
                cIdx = ++cIdx % items.length;
                try {
                    sleep(ConstantPool.THREADTIMEOUT);
                } catch (InterruptedException ex) {
                    ui.reportError(ex);
                }
            }
        }
    }
}
