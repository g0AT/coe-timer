
import data.ConstantPool;
import grafics.Gui;
import logic.SoundPlayer;
import logic.Timerthread;
import logic.UserInterface;

public class Main {

    public static void main(String[] args) throws InterruptedException {
//        UserInterface ui = new UIConsole();
        UserInterface ui = new Gui();
        SoundPlayer player = new SoundPlayer(ConstantPool.AUDIOPATH, ui);
        Timerthread tt = new Timerthread(ui, player);
        tt.start();
        tt.join();
    }
}
